// Constants
nm := 1e-9

// Discretization
dx := nm
dy := nm
dz := 0.4 * nm

// Dimensions
width := 80 * nm
height := 0.4 * nm

// Setup
SetGridSize (Floor (width / dx), Floor (width / dy), Floor (height / dz))
SetCellSize (dx, dy, dz)

// Geometry
SetGeom (Circle(width))

// Inputs
Dmin := 2e-3
Dmax := 6e-3
Dstep := 0.2e-3

// Parameters
Msat = 5.8e5
Aex = 1.5e-11
alpha = 0.3
AnisU = Vector (0, 0, 1)
Ku1 = 0.8e6
Temp = 0

// Simulation
for D := Dmin; D < Dmax + (Dstep / 2); D += Dstep {
	// Calculate and print progress
	P := Floor (100 * (D - Dmin) / (Dmax - Dmin))
	Print (sprintf("[%3.0f%%] Running with Dind=%.05f", P, D))

	// Setup
	m = NeelSkyrmion (1, -1)
	Dind = D

	// Run
	Relax ()
	Minimize ()

	// Save
	SaveAs (m, sprintf("relax_m_Dind=%.05f.ovf", D))
}
