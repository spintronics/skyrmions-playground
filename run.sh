#!/bin/sh

do_main ()
{
	# Paths
	readonly rootdir="$(realpath "$(dirname "$0")")"
	readonly input="${rootdir}/src/relax.mx3"
	readonly outdir="${rootdir}/out/relax.out"
	readonly pngzdir="${outdir}/pngz"
	readonly pngdir="${outdir}/png"

	# Programs
	readonly mumax="${MUMAX:-mumax3}"
	readonly mconv="${mumax}-convert"
	readonly detect="${rootdir}/tools/detect-skyrmions.py"

	# Prepare
	mkdir -p "${pngdir}" "${pngzdir}"

	# Run
	${mumax} -o "${outdir}" "${input}"

	# Convert
	readonly ovfs="$(find "${outdir}" -iname '*.ovf' | tr '\n' ' ')"
	${mconv} -png -o "${pngdir}" ${ovfs}
	${mconv} -png -comp z -o "${pngzdir}" ${ovfs}

	# Detect
	readonly pngs="$(find "${pngzdir}" -iname '*.png' | tr '\n' ' ')"
	for png in ${pngs}; do
		echo "Detecting skyrmions in ${png}..."
		python3 ${detect} ${png}
	done

	echo "Done."
	echo
}

do_main "$@"
