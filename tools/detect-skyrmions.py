#!/usr/bin/env python3

import sys
import cv2
import numpy as np

def detect_hough (img):
    # Grayscale
    gray = cv2.cvtColor (img, cv2.COLOR_BGR2GRAY)

    # Blue using 3x3 kernel
    blurred = cv2.blur (gray, (3, 3))

    # Hough transform on blurred image
    res = cv2.HoughCircles (blurred,
                            cv2.HOUGH_GRADIENT,
                            1,
                            20,
                            param1 = 50,
                            param2 = 30,
                            minRadius = 1,
                            maxRadius = 38)

    if res is None:
        return None

    circles = np.uint16 (np.around (res))
    for c in circles[0, :]:
        x, y, r = c[0], c[1], c[2]

        cv2.circle (img, (x, y), r, (0, 255, 0), 2)
        cv2.circle (img, (x, y), 1, (0, 0, 255), 3)

    return img


def print_usage (name):
    print ('Usage: {} ZCOMP'.format(name))


def do_main ():

    if len(sys.argv) < 2:
        print_usage (sys.argv[0])
        return 1

    res = 1
    imgpath = sys.argv[1]

    try:
        img = cv2.imread (imgpath, cv2.IMREAD_COLOR)
        img = detect_hough (img)

        if img is not None:
            cv2.imwrite (imgpath, img)
            res = 0
    except (cv2.error):
        print ('Something bad happened')
        pass

    sys.exit (res)


if __name__ == '__main__':
    do_main ()
